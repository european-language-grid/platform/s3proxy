package org.gaul.s3proxy;

import com.google.gson.Gson;
import okhttp3.*;
import org.eclipse.jetty.server.Request;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.rest.AuthorizationException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public class S3ProxyHandlerELG extends S3ProxyHandlerJetty {

    private static final Logger logger = LoggerFactory.getLogger(
            S3ProxyHandlerELG.class);
    public static final String NOT_ALLOWED = "not allowed";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String PUT = "put";
    public static final String NOTIFY = "notify";
    public static final String BACKEND_RESPONSE_INVALID = "Backend Response invalid";
    public static final String ELG_RESOURCE_ID = "ELG-RESOURCE-ID";
    private static final String BUCKET_NOT_ALLOWED = "The targeted bucket is not permitted";
    private static final String ID_DOES_NOT_MATCH = "RequestId and BackendId do not match.";
    private final String elgAPIKey;
    private final List<String> elgAllowedBuckets;
    private final String realS3Path;
    private List<String> elgAllowedMethods;
    private String elgValidationUrl;

    private final String[] passThroughHeaderNames = {"scope", "filetype", "resourceid", "filename", AUTHORIZATION_HEADER};
    private final List<String> passThroughHeaders = Arrays.asList(passThroughHeaderNames);


    S3ProxyHandlerELG(BlobStore blobStore, AuthenticationType authenticationType, String identity, String credential, @Nullable String virtualHost,
                      long v4MaxNonChunkedRequestSize, boolean ignoreUnknownHeaders, CrossOriginResourceSharing corsRules, String servicePath,
                      int maximumTimeSkew, String elgAllowedMethods, String elgValidationUrl, String elgAPIKey, String elgAllowedBuckets, String realS3Path) {
        super(blobStore, authenticationType, identity, credential, virtualHost, v4MaxNonChunkedRequestSize, ignoreUnknownHeaders, corsRules, servicePath, maximumTimeSkew);
        this.elgAllowedMethods = Arrays.asList(elgAllowedMethods.split(","));
        this.elgValidationUrl = elgValidationUrl;
        this.elgAPIKey = elgAPIKey;
        this.elgAllowedBuckets = Arrays.asList(elgAllowedBuckets.split(","));
        this.realS3Path = realS3Path;
    }

    public HttpServletRequestWrapper createRequestWrapper(HttpServletRequest request, String newUrl) {
        final HttpServletRequestWrapper wrapped = new HttpServletRequestWrapper(request) {
            @Override
            public String getRequestURI() {
                return newUrl;
            }
        };
        return wrapped;
    }

    @Override
    public void handle(String target, Request baseRequest,
                       HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        try {
            //ask Backend Server to authorize upload request
            String body = this.authorizeUpload(baseRequest);
            Gson gson = new Gson();
            ELGAuthorizationResponse authResponse = gson.fromJson(body, ELGAuthorizationResponse.class);

            //Change upload location etc. to the values from the backend response
            if (authResponse.bucket == null || authResponse.filename == null || authResponse.id == null){
                throw new AuthorizationException(BACKEND_RESPONSE_INVALID);
            }
            if (!elgAllowedBuckets.contains(authResponse.bucket)){
                throw new AuthorizationException(BUCKET_NOT_ALLOWED);
            }
            //The request will be directed at the configured BlobStore, the domain plays no further role
            String newTarget = "/"+authResponse.bucket+"/"+authResponse.filename;
            HttpServletRequestWrapper wrapper = createRequestWrapper(request, newTarget);
            logger.debug("Re-routing upload request to "+newTarget);
            //actual invocation to S3
            super.handle(newTarget, baseRequest, wrapper, response);
            //notify BE server about completed upload

            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                this.notifyUpload(baseRequest, response, authResponse.bucket, authResponse.filename);
                response.addHeader("Location", getUrl(authResponse.bucket, authResponse.filename).toString());
            }
            else{
                logger.error(response.toString());
            }

        } catch (Exception e) {
            throw e;
        }
    }

    private String notifyUpload(Request baseRequest, HttpServletResponse response, String bucket, String filename) throws IOException {
        for (String header: response.getHeaderNames()){
            logger.debug("Header: " + header);
        }

        Map<String, String> data = new HashMap<>();
        data.put("LastModified", getHeader(response, "Date"));
        data.put("Content-Length", getHeader(baseRequest,"Content-Length"));
        data.put("ETag", getHeader(response, "ETag"));
        data.put("Location", getUrl(bucket, filename));


        ELGUploadFinalizeRequest fr = buildELGUploadFinalizeRequest(baseRequest, data);
        Map<String, String> headers = getPassThroughHeaders(baseRequest);

        return httpPut(getId(baseRequest), fr, headers);
    }

    @NotNull
    private String getUrl(String bucket, String filename) {
        return this.realS3Path+'/'+bucket+'/'+filename;
    }

    private String authorizeUpload(Request baseRequest) throws IOException {
        if (!elgAllowedMethods.contains(baseRequest.getMethod().toUpperCase())) {
            throw new AuthorizationException(NOT_ALLOWED);
        }
        Map<String, String> headers = getPassThroughHeaders(baseRequest);
        ELGAuthorizationRequest req = buildValidationRequest();
        String resourceId = getId(baseRequest);
        return httpPut(resourceId, req, headers);

    }

    @NotNull
    private Map<String, String> getPassThroughHeaders(Request baseRequest) {
        Map<String, String> headers = new HashMap<>();
        for (String header : passThroughHeaders){
            if (baseRequest.getHeader(header) != null) headers.put(header, baseRequest.getHeader(header));
        }
        return headers;
    }

    private String getHeader(Request baseRequest, String header) {
        String value = baseRequest.getHeader(header);
        if (value == null) throw new AuthorizationException("Header needed: "+header);
        return value;
    }
    private String getHeader(HttpServletResponse response, String header) {
        String value = response.getHeader(header);
        if (value == null) throw new AuthorizationException("Header needed: "+header);
        return value;
    }

    private String getId(Request request){
        String id= request.getHeader(ELG_RESOURCE_ID);
        if (id==null){
            throw new AuthorizationException("Header needed: "+ELG_RESOURCE_ID);
        }
        return id;
    }
    private ELGAuthorizationRequest buildValidationRequest() {
        ELGAuthorizationRequest ar = new ELGAuthorizationRequest();
        ar.action = PUT;
        ar.api_key = this.elgAPIKey;
        return ar;
    }
    private ELGUploadFinalizeRequest buildELGUploadFinalizeRequest(Request baseRequest, Map<String, String> data){
        ELGUploadFinalizeRequest ar = new ELGUploadFinalizeRequest();
        ar.action = NOTIFY;
        ar.api_key = this.elgAPIKey;
        ar.data = data;
        return ar;
    }

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    String httpPut(String resourceId, ELGAuthorizationRequest payload, Map<String,String> headers) throws IOException {
        try {
            String url = this.elgValidationUrl+resourceId+"/";
            OkHttpClient client = new OkHttpClient();
            Gson gson = new Gson();
            String json = gson.toJson(payload);
            RequestBody body = RequestBody.create(json, JSON);
            okhttp3.Request.Builder builder= new okhttp3.Request.Builder()
                    .url(url)
                    .post(body);
            for (String key: headers.keySet()){
                builder.header(key, headers.get(key));
            }
            okhttp3.Request request = builder.build();

            logger.debug("Backend Request: " + request.toString() + "/" + json);

            Response response = client.newCall(request).execute();
            String responseBody = response.body().string();
            logger.debug("Backend Response: " + "/" + response.code() + "/" + responseBody);
            if (response.code() != 200) {
                throw new AuthorizationException(NOT_ALLOWED);
            }
            return responseBody;
        } catch (IllegalArgumentException e) {
            throw new AuthorizationException(NOT_ALLOWED);

        }
    }


    class ELGAuthorizationRequest {
        public String action;
        public String api_key;
    }

    class ELGAuthorizationResponse {
        public Integer id;
        public String filename;
        public String bucket;
    }

    class ELGUploadFinalizeRequest extends ELGAuthorizationRequest{
        public Map<String,String> data;
    }
};


