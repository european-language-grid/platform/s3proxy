# S3Proxy

- This is a fork of https://github.com/gaul/s3proxy, with ELG specific changes
- See original readme for usage details: https://github.com/gaul/s3proxy/blob/master/README.md


# Additions

Additional parameters are available to configure via properties files or env vars:

- s3proxy.elg.backend-validation-url : backend URL to call to authorize request
- s3proxy.elg.allowed-methods : comma separated list of of http methods to proxie
- s3proxy.elg.allowed-buckets : comma separated list of  allowed buckets
- s3proxy.elg.api-key : api key to call backend api with



