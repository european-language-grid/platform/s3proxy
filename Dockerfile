# Multistage - Builder
FROM maven:3.5.0-jdk-8-alpine as s3proxy-builder

WORKDIR /opt/s3proxy
COPY . /opt/s3proxy/

RUN mvn package -DskipTests

# Multistage - Image
FROM openjdk:8-jre-alpine

WORKDIR /opt/s3proxy

COPY \
    --from=s3proxy-builder \
    /opt/s3proxy/target/s3proxy \
    /opt/s3proxy/src/main/resources/run-docker-container.sh \
    /opt/s3proxy/

EXPOSE 80

ENTRYPOINT ["/opt/s3proxy/run-docker-container.sh"]
